from hannah.lib import messaging
import json
import time


'''

    TODO: Make mock for testing without mqtt broker#

'''



def tvvvest_mess_conf():

    def on_recieved(subscribable):
        print (subscribable.sub_topic+':'+subscribable.sub_value.__str__())
            
    cfg = {'host':'localhost','topics':['#'],'log_level':'DEBUG','log_stderr':True}
    
    c = messaging.client(config=cfg)
    with c: 
        s = messaging.subscribable('test12',client=c)
        s.on_recieved = on_recieved

        p = messaging.publishable('test12',client=c)
        p.pub_value = "miau"
        
        c.join()
        
    for err in c.errors:
        raise err




def tecffst_json():
    
    meta = { 'name': '/some/test', 'title':'blahblah' , 
            'input': { 'path': 'hugen/dubel',
                       'title': 'jkshnceldh',
                       
                    }
            
            }
    print (json.dumps(meta,indent=2))
  
  
  
def test_meta():
    cfg = {'host':'localhost','topics':['#'],'log_level':'DEBUG','log_stderr':True}
    c = messaging.client(config=cfg)
    with c:
        meta = messaging.interfaces('test',client=c,config=cfg)
        time.sleep(4)
        print (meta.interfaces)
        c.join()
        
    for err in c.errors:
        raise err
    
    
# moved from test_config  
#def csvregex_test():
#    item = hc.ConfigItemCSVRegex("test", "description", "a|b")
#    _mkConfigurable(item, testvalue='a,"b "')      
#           
#def csvregex_nomatch_test():
#    item = hc.ConfigItemCSVRegex("test", "description", "a|b")
#    try:
#        _mkConfigurable(item, testvalue='a,"b ",c')
#    except(hc.ConfigInvalidException) as e:
#        assert 'Value c does not match the Regular Expression' in e.__str__()   
