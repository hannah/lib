import hannah.lib as hannah


#a button that switches on an alarm light for a specified amount of time

#Meta

#name /hq/kueche/alarm
#title Alarm
#input 
#    path    /ccc-ffm/hq/kueche/alarm/set
#    data    string,[off,on,range(int,1-3600)]
#    fields  nick,set
#    default 10
#output    
#    path    /ccc-ffm/hq/kueche/alarm
#    data    [off,on]


#name /hq/power
#output
#    path    /ccc-ffm/hq/power
#    unit    watt
#    frequency 10
#    tolerance 0.1


#name /hq/lounge/sound/power
#title Stereo
#input
#    path   /ccc-ffm/hq/lounge/stereo/power/set
#    data    [on,off]
#output
#    path    /ccc-ffm/hq/lounge/stereo/power
#    data    [on,off]

#name /hq/lounge/beamer/power
#title Beamer
#input
#    path   /ccc-ffm/hq/lounge/beamer/power/set
#    data    [on,off]

#output
#    path    /ccc-ffm/hq/lounge/beamer/power
#    data    [on,powerdown,off]

#translation [
#    on:An
#    off:Aus
#    powerdown:Abkühlen



#    title - see above
#    data - describes the dataset transported in the mqtt payload. As a rule the payload should only contain a single "thing". A single Value, A single command. Dont re-purpose or use "special" values. Use separate topics for that.
 ##           int/long/string/bytes/range(type,from,to)  -- TODO: range definition via regex?
 #           listof possible values: [on,off,toggle]/[up,down,toggle-mute]([range(int,0-11),mute,unmute]
 #                           
#    unit - The unit a value is in (use SI names where possible)
#    frequency - may be present to describe the expected frequency of new messages in seconds.
#    tolerance - useful if a numerical value is transported to determine accuracy or significance of a change of value.
#    represent - a hint how to represent this in a User Interface: (TODO: is there a nice standard for this? HTML forms?)
#        for input
#            noshow - dont show in UI
#            txt - a text box
#            txt.WIDTH - a text box with WIDTH characters width
#            edit - a text editor
#            edit.WIDTH.HEIGHT - WIDTH characters wide and HEIGHT lines
#            tick.single - single select check boxes
#            tick.multi (or just tick) - multi select check boxes
#            drop or drop.single - single select drop down
#            drop.multi - multi select dropdown
#            dial - a value dial
#            slide - a value slider
#            button - a button with a single value
#            buttons - a single value button for every possible value
#            toggle - a button that changes state (on/off, VGA/DVI/HDMI)
#            switch - a/b switch
            
#        for output
#            noshow - dont show in UI
#            gauge - 
#            traffic - traffic lights 
            
#    translations - like title but for the possible values in the data
 