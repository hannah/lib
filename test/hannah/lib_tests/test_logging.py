
import logging
import hannah.lib.log as hl
import hannah.lib.config as hc

class loggable_handler(logging.Handler):

    def __init__(self, loggable=hl.loggable()):
        self.loggable = loggable
        hl.log_to_stderr(self.loggable.log, logging.DEBUG)
        self.record = None
        logging.Handler.__init__(self)

    def __enter__(self):
        self.loggable.log.addHandler(self)
        return self.loggable
    
    def __exit__(self, exc_type, exc_value, traceback):
        self.loggable.log.removeHandler(self)

    def emit(self, record):
        self.record = record
        

def _level_msg(func, handler, level, msg):
    func(msg)
    assert handler.record.levelno == level
    assert handler.record.msg == msg     
        
def test_msg_logging():
    handler = loggable_handler()
    with handler as loggable:
        yield _level_msg, loggable.debug, handler, logging.DEBUG, 'a debug msg'
        yield _level_msg, loggable.info, handler, logging.INFO, 'a info msg'
        yield _level_msg, loggable.warning, handler, logging.WARN, 'a warn msg'
        yield _level_msg, loggable.error, handler, logging.ERROR, 'a error msg'
        yield _level_msg, loggable.critical, handler, logging.CRITICAL, 'a critical msg'
       
        
def test_exception_logging():
    handler = loggable_handler()
    with handler as loggable:
        msg = 'an exception'
        try:
            raise Exception(msg)
        except(Exception) as e:
            loggable.exception(e)
        assert handler.record.levelno == logging.ERROR
        assert isinstance(handler.record.msg, Exception)
        assert handler.record.msg.__str__() == msg
        
def configurable_with_logging_notitems_test():
    class loggable_configurable(hl.loggable,hc.Configurable):
        pass
        
    lc = loggable_configurable()
    handler = loggable_handler(loggable=lc)
    with handler as loggable:
        loggable.CONF_ITEMS = []
        loggable.configure()
        
        #just trigger the creation dont log anything so that the expected info message is in the handler
        loggable.log
        assert handler.record.levelno == logging.INFO
        assert handler.record.msg == 'This class is of type configurable and loggable but does not provide logging config items'
       
def configurable_with_logging_defaultitems_test():
    class loggable_configurable(hl.loggable,hc.Configurable):
        CONF_ITEMS = hl.loggable.CONF_ITEMS
    lc = loggable_configurable()
    handler = loggable_handler(loggable=lc)
    with handler as loggable:
        
        loggable.configure()
        #just trigger the creation dont log 
        loggable.log
        assert handler.record is None
        loggable.debug('test')
        assert handler.record is None
        _level_msg(loggable.warning, handler, logging.WARN, 'a warning msg')
   
       
        