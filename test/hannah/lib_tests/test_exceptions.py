from hannah.lib.exceptions import MultipleExceptions

def multiexeption_test():
    try:
        with MultipleExceptions('Multi Exception') as me:
            with me:
                raise Exception('First Exception')
            with me:
                raise Exception('Second Exception')
            with me:
                raise Exception('Third Exception')            
        assert False
    except(MultipleExceptions) as multi:
        msg = multi.__str__()
        assert 'MultipleExceptions: Multi Exception' in msg
        assert '\tException: First Exception' in msg    
        assert '\tException: Second Exception' in msg
    
    
def multi_in_multiexception_test():
    try:
        with MultipleExceptions('Top Level') as me:
            with me:
                raise Exception('First in First')
            with me:
                with MultipleExceptions('Second Level') as me2:
                    with me2:
                        raise (Exception('Second in Second'))
            with me:
                raise (Exception('Second in First'))
        assert False
    except(MultipleExceptions) as multi:
        msg = multi.__str__()
        print(multi)
        assert 'MultipleExceptions: Top Level' in msg
        assert '\tException: First in First' in msg    
        assert '\tMultipleExceptions: Second Level' in msg    
        assert '\t\tException: Second in Second' in msg
        assert '\tException: Second in First' in msg
        