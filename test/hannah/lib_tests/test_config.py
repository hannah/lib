import hannah.lib.config as hc
from nose.tools.nontrivial import raises
import logging

# TODO: write tests for signals
# TODO: write tests for file reads
# TODO: write tests for ENV values
# TODO: write tests for interpolation
# TODO: write test for cache thread safety


    
#def test_Configurable_NoItems():
#    class testclass(hc.Configurable):
#        def __init__(self):
#            self.configure()
#    c = testclass()

    
def _mkConfigurable(item, testvalue='missing'):
    class testclass(hc.Configurable):
        
        def __init__(self):
            self.configure(items=[item],read=False,validate=False)
            self.conf.read()
            if testvalue != 'missing':
                vo = self.conf._items[item.name]
                vo.value = testvalue
            self.conf.validate()
       
    tc = testclass()   
    return tc                 

def _basecheck_configitem_mandatory_novalue(itemtype):
    i = itemtype("test","description", mandatory=True)
    try:
        _mkConfigurable(i)
    except(hc.ConfigInvalidException) as e:
        assert 'Mandatory value missing' in e.__str__()


def _basecheck_configitem_multivalue_single(itemtype,value):
    i = itemtype("test", "description1", multivalue=True)
    try:
        _mkConfigurable(i,value)
    except(hc.ConfigInvalidException) as e:
        assert 'Not a list of values' in e.__str__()

def _basecheck_configitem_multivalue(itemtype,value):
    i = itemtype("test", "description1", multivalue=True)
    _mkConfigurable(i,[value,value])

def _basecheck_configitem_multivalue_wrongtype(itemtype,value):
    class wrongtype():
        pass
    i = itemtype("test", "description1", multivalue=True)
    try:
        _mkConfigurable(i,[value,value,wrongtype()])
    except(hc.ConfigInvalidException) as e:
        assert 'not of type' in e.__str__()  

def _basecheck_configitem_defaultvalue(itemtype,value,result=None):
    
    i = hc.ConfigItem("test", "description1", default=value)
    c = _mkConfigurable(i)
    assert  c.conf.test == value

def _basecheck_configitem_hasvalue(itemtype,value,result=None):
    if not result:
        result = value
    i = itemtype("test", "description1")
    c = _mkConfigurable(i,value)
    assert  c.conf.test == result


def _basecheck_configitem_wrongtype(itemtype):
    
    class wrongtype():
        pass
    
    i = itemtype("test", "description1")
    try:
        _mkConfigurable(i,wrongtype())
    except(hc.ConfigInvalidException) as e:
        assert 'not of type' in e.__str__()   


def check_configItem_basic(itemtype,value, wrongtype=True, result=None):
    '''
    Generate basic tests for ConfigItems
    :param itemtype: ConfigItem class to be tested
    :type itemtype: class
    :param value:A valid value for the item
    :type value:Correct Type for item
    '''
    yield _basecheck_configitem_defaultvalue,itemtype, value, result
    yield _basecheck_configitem_hasvalue,itemtype, value, result
    yield _basecheck_configitem_mandatory_novalue,itemtype
    yield _basecheck_configitem_multivalue_single,itemtype, value
    yield _basecheck_configitem_multivalue,itemtype, value
    yield _basecheck_configitem_multivalue_wrongtype,itemtype, value
    
    if wrongtype:
        yield _basecheck_configitem_wrongtype,itemtype

  
def test_configitem():
    for test in check_configItem_basic(hc.ConfigItem, 'test', False):
        yield test
    
def test_configitem_boolean():
    for test  in check_configItem_basic(hc.ConfigItemBoolean, True):
        yield test

def test_configitem_string():
    for test  in check_configItem_basic(hc.ConfigItemString, 'miau'):
        yield test
        
def test_configitem_int():
    for test  in check_configItem_basic(hc.ConfigItemInt, 123):
        yield test

def test_configitem_float():
    for test  in check_configItem_basic(hc.ConfigItemFloat, 123.345):
        yield test

def test_configitem_csv():
    for test in check_configItem_basic(hc.ConfigItemCSV, 'a,"b ",c',wrongtype=False,result=['a','b','c']):
        yield test

       
def test_configitem_loglevel():
    for test in check_configItem_basic(hc.ConfigItemLogLevel, logging.DEBUG):
        yield test
    
@raises(hc.ConfigInvalidException)    
def invalid_loglevel_int_test():
    item = hc.ConfigItemLogLevel("test","description")
    _mkConfigurable(item, 353425254)
 
@raises(hc.ConfigInvalidException)    
def invalid_loglevel_string_test():
    item = hc.ConfigItemLogLevel("test","description")
    _mkConfigurable(item, 'wrong')
   
def valid_loglevel_int_test():
    item = hc.ConfigItemLogLevel("test","description")
    _mkConfigurable(item, logging.DEBUG)
    _mkConfigurable(item, logging.ERROR)
    _mkConfigurable(item, logging.WARN)
    _mkConfigurable(item, logging.INFO)
    
def valid_loglevel_string_test():
    item = hc.ConfigItemLogLevel("test","description")
    _mkConfigurable(item, 'DEBUG')
    _mkConfigurable(item, 'WARN')

def csvregex_test():
    item = hc.ConfigItemCSVRegex("test", "description", "a|b")
    _mkConfigurable(item, testvalue='a,"b "')      
           
def csvregex_nomatch_test():
    item = hc.ConfigItemCSVRegex("test", "description", "a|b")
    try:
        _mkConfigurable(item, testvalue='a,"b ",c')
    except(hc.ConfigInvalidException) as e:
        assert 'Value c does not match the Regular Expression' in e.__str__()   
           
def subconfig_empty_test():
    subconfitems = [hc.ConfigItemInt('subitem','subitem description')]
    subconf = {} 
    item = hc.ConfigItemSubConfig("test","description",subconfitems)
    _mkConfigurable(item, testvalue=subconf)
    
def subconfig_empty_mandatory_test():
    subconfitems = [hc.ConfigItemInt('subitem','subitem description',mandatory=True)]
    subconf = {} 
    item = hc.ConfigItemSubConfig("test","description",subconfitems)
    try:
        _mkConfigurable(item, testvalue=subconf)
    except(hc.ConfigInvalidException) as e:
        assert 'Not a Valid Subconfig' in e.__str__() 
    
def subconfig_ok_mandatory_test():
    subconfitems = [hc.ConfigItemInt('sub','subitem description',mandatory=True)]
    subconf = {'sub':123} 
    item = hc.ConfigItemSubConfig("test","description",subconfitems)
    c = _mkConfigurable(item, testvalue=subconf)
    assert c.conf.test.sub == 123
    