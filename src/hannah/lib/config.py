'''
Created on 21 Apr 2015

TODO: really implement this? Isnt there something. crumbs looks like a good candidate
TODO: implement a cache/singleton mechanisms 

# some configuration will be stored in the message broker. so there is an chicken/egg thing here


@author: wonko
'''
import logging
import csv
import re
import json
from hannah.lib.exceptions import MultipleExceptions


class ConfigurationException(MultipleExceptions):
    pass

class ConfigInvalidException(ConfigurationException):
    pass

__cache__ = None

class ConfigValue(object):
       
    # TODO: Implement __eq__
    
    def __init__(self, item, value=None):
        self.item = item
        self._validated = False
        self._valid = False
        if value:
            self._value = value
        else:
            self._value = self.item.default
        
    def validate(self):
        try:
            self.item.validate(self._value)
            self._valid = True
            self._validated = True
        except (Exception) as e:
            self._valid = False
            raise e
    
    @property
    def value(self):
        if self._validated and self._valid:
            return self._value
        else:
            raise ConfigurationException('Item ' + self.item.name + ' read before validation')
        
    @value.setter
    def value(self, value):
        self._validated = False
        self._valid = False
        self._value = self.item.deserialize(value)
        
    # todo: implement repr to serialize
    
class ConfigItem(object):
    '''
    
    '''
    
    ITEM_TYPE = 'bytes'
    ITEM_TYPE_CLASS = object

    '''
    The base type of this item. Here bytes aka we dont know.
    '''
    
    def _is_valid_type(self, value):
        if not isinstance(value, self.ITEM_TYPE_CLASS):
            raise ConfigInvalidException('Value ' + value.__str__() + ' not of type ' + self.ITEM_TYPE)
    
    def validate(self, value):
        with ConfigInvalidException('Item ' + self.name + ' is Invalid') as invalid:
            with invalid:
                if self.mandatory and not value:
                    raise ConfigInvalidException('Mandatory value missing')
            with invalid:
                if self.multivalue:
                    if isinstance(value, list) or isinstance(value, dict):
                        for v in value:
                            with invalid:
                                self._is_valid_type(v)
                                
                    else:
                        raise ConfigInvalidException('Not a list of values')
                elif value:
                    self._is_valid_type(value)
          
    
    def __init__(self, name, description, default=None, mandatory=False, multivalue=False):
        self.name = name
        self.description = description
        self.mandatory = mandatory
        self.multivalue = multivalue
        self.default = default
    
    def deserialize(self, value):
        return value
    
    def serialize(self, value):
        return value
        
    def __hash__(self, *args, **kwargs):
        return object.__hash__(self.name, *args, **kwargs)
    
    def __str__(self, *args, **kwargs):
        return self.name
  
    
class ConfigItemBoolean(ConfigItem):
    
    ITEM_TYPE = 'bool'
    ITEM_TYPE_CLASS = bool

class ConfigItemString(ConfigItem):
    
    ITEM_TYPE = 'string'
    ITEM_TYPE_CLASS = str
    
class ConfigItemInt(ConfigItem):
    ITEM_TYPE = 'int'
    ITEM_TYPE_CLASS = int
    
class ConfigItemFloat(ConfigItem):
    ITEM_TYPE = 'float'
    ITEM_TYPE_CLASS = float
    
class ConfigItemLogLevel(ConfigItem):
    ITEM_TYPE = 'Log Level'
    ITEM_TYPE_CLASS = int
    
    def deserialize(self, value):
        try:
            return logging._checkLevel(value)
        # returning the raw value for validate to pick up
        except:
            return value
        
    def validate(self, value):
        with ConfigInvalidException('Not a Valid Loglevel') as invalid:
            with invalid:
                ConfigItem.validate(self, value)
        
            with invalid:
                if self.multivalue:
                    for val in value:
                        logging._levelToName[val]
                else:
                    logging._levelToName[value]
        
class ConfigItemCSV(ConfigItemString):
    ITEM_TYPE = 'Comma Separated Values'
    
    def __init__(self, name, description, default=None, mandatory=False, multivalue=True):
        ConfigItemString.__init__(self, name, description, default=default, mandatory=mandatory, multivalue=multivalue)
    
    def deserialize(self, value):
        try:
            values = []
            for row in csv.reader([value]):
                for val in row:
                    values.append(val.strip())
            return values
        except:
            return value
        
    def validate(self, value):
        with ConfigInvalidException('Not a Valid CSV') as invalid:
            with invalid:
                ConfigItemString.validate(self, value)
            with invalid:
                if not self.multivalue:
                    raise ConfigInvalidException('CSV Item not flagged multivalue')
                
class ConfigItemRegex(ConfigItemString):
    ITEM_TYPE = 'Regular Expression Match'

    def __init__(self, name, description, re_pattern, re_flags=0, default=None, mandatory=False, multivalue=False):
        ConfigItemString.__init__(self, name, description, default=default, mandatory=mandatory, multivalue=multivalue)
        self.pattern = re.compile(re_pattern, re_flags)
    
    def validate(self, value):
        def match(pattern, value):
            match = self.pattern.match(value)
            if not match:
                raise ConfigInvalidException('Value ' + value.__str__() + ' does not match the Regular Expression')
            
        with ConfigInvalidException('Not a valid Regex Match') as invalid:
            with invalid:
                ConfigItemString.validate(self, value)
            if self.multivalue:
                for val in value:
                    with invalid:
                        match(self.pattern, val)
            elif value:
                with invalid:
                    match(self.pattern, val)
           
    
class ConfigItemCSVRegex(ConfigItemCSV, ConfigItemRegex):
    def __init__(self, name, description, re_pattern, re_flags=0, default=None, mandatory=False, multivalue=True):
        ConfigItemCSV.__init__(self, name, description, default=default, mandatory=mandatory, multivalue=multivalue)
        ConfigItemRegex.__init__(self, name, description, re_pattern, re_flags, default=default, mandatory=mandatory, multivalue=multivalue)        
    
    def validate(self, value):
        with ConfigInvalidException('Not a valid CSVRegex Match') as invalid:
            with invalid:
                ConfigItemCSV.validate(self, value)
            with invalid:
                ConfigItemRegex.validate(self, value)
              
class Configuration(object):
    '''
    Configuration retrieved from multiple sources (in order of reading, last one wins):
        * Default system config path/files
        * user config path/files
        * Environment variables (the config paths are read before obviously)
        * Command line arguments
        * configuration topics in message broker
    '''
    
    SOURCE_FILES = "files"
    SOURCE_ENV = "env"
    
    def __init__(self, name, items={}, description={}):
        self.name = name
        self.description = description
        self.__complete = False
        self.valid = False
        self.on_error = None
        self.on_ready = None
        self._items = {}
        if items:
            for item in items:
                self._items[item.name] = ConfigValue(item)
        
        
    def _value_obj(self, name):
        if name in self._items:
            return self._items[name]
        else:
            return None
        
        
          
    def __getattr__(self, name):
        if not self.valid:
            self.__raise_or_call(ConfigInvalidException('Config ' + self.name + ' The Configuration has not been validated yet'))
            return None
        
        vo = self._value_obj(name)
        if vo:
            return vo.value
        else:
            self.__raise_or_call(AttributeError('Could not find Configuration Item ' + name + '. It has not been configured'))
            return None
            
    def __raise_or_call(self, exc):
        if self.on_error:
            self.on_error(exc)
            return None, None
        else:
            raise exc
    
    def validate(self, force=False):
        if self.valid and not force:
            return self.valid
            
        # TODO: Make async but only if wished for
        
        try:
            with ConfigInvalidException('Validation of Configuration failed') as invalid:
                for value in self._items.values():
                    with invalid:
                        value.validate()
            self.valid = True
            if self.on_ready:
                self.on_ready()
            return self.valid
        except(ConfigInvalidException) as cie:
            self.__raise_or_call(cie)
            return False
        
    
            
            
    def read(self, cache=True, use=[SOURCE_FILES,SOURCE_ENV], config={}):

        cached_conf = None
        if __cache__ and cache and self.name in __cache__:
            cached_conf = __cache__[self.name]
                
        for name, value in self._items.items():
            # Use the cache
            if cached_conf:
                if name in cached_conf:
                    value.value = cached_conf[name]
                    
            # TODO: Read configuration from all over the place
            # TODO: Extend config with remote config
        
            # hardcoded config
            if name in config:
                value.value = config[name]
            
        # write the cache


    def _deserialize_json(self,json_string):
        cfg_json = json.loads(json_string)
        return cfg_json
        
        

class ConfigItemSubConfig(ConfigItem):
    ITEM_TYPE = 'Configuration'
    ITEM_TYPE_CLASS = Configuration
    
    def __init__(self, name, description, config_items, default=None, mandatory=False, multivalue=False):
        ConfigItem.__init__(self, name, description, default=default, mandatory=mandatory, multivalue=multivalue)
        self._config_items = config_items
    
    def deserialize(self, value):
        try:
            conf = Configuration(self.name, items=self._config_items, description=self.description)
            conf.read(cache=False, use=[], config=value)
            return conf
        except Exception as e:
            print(e)
            return value
        
    def validate(self, value):
        with ConfigInvalidException('Not a Valid Subconfig') as invalid:
            with invalid:
                ConfigItem.validate(self, value)
            with invalid:
                if value:
                    value.validate(force=True)


class Configurable(object):
    '''
    Base Class to mark an Object configurable.
    '''
    
    CONF_NAME = None
    CONF_DESCRIPTION = None
    CONF_ITEMS = None
    
    def configure(self, name=None, items=None, description=None, read=True, validate=True, config={}):
        
        if not name:
            name = self.CONF_NAME
        if not name:
            name = self.__module__ + '.' + self.__class__.__name__
        
        if not items:
            items = self.CONF_ITEMS
            
        if not description:
            description = self.CONF_DESCRIPTION
            
        self.conf = Configuration(name, items=items, description=description)
        
        if hasattr(self, 'on_conf_error'):
            self.conf.on_error = self.on_conf_error
        if hasattr(self, 'on_conf_ready'):
            self.conf.on_ready = self.on_conf_ready
            
        if read:
            self.conf.read(config=config)
        if validate:
            self.conf.validate()
        
        
