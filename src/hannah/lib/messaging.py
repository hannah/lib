'''
Created on 21 Apr 2015

@author: wonko
'''

from hannah.lib.log import loggable
from hannah.lib.config import Configuration, ConfigInvalidException, Configurable, ConfigItemInt, ConfigItemString, \
    ConfigItemSubConfig, ConfigItemCSV, \
    ConfigItemCSVRegex, ConfigItemFloat
import paho.mqtt.client as mc
from threading import Thread, Event, Lock

import re
from builtins import str

class NoClientException(Exception):
    pass

class SubscribeOnlyException(Exception):
    pass

class PublishException(Exception):
    pass

class NotConnectedException(PublishException):
    pass
     
class InterfaceDataTypes(ConfigItemCSVRegex):
    
    TYPES_RE = 'int|long|string|bytes|timestamp|json|range\(.+\)|^\[.+\]$'
    TYPES_RE_FLAGS = re.IGNORECASE
    
    def __init__(self, name, description, default=['string'], mandatory=False, multivalue=True):
        ConfigItemCSVRegex.__init__(self, name, description, self.TYPES_RE, self.TYPES_RE_FLAGS, default=default, mandatory=mandatory, multivalue=multivalue)

class InterfaceRepresentation(ConfigItemCSVRegex): 
    REPRESENTATION_RE = 'noshow|1line|'
    REPRESENTATION_RE_FLAGS = re.IGNORECASE       

    def __init__(self, name, description, default=None, mandatory=False, multivalue=True):
        ConfigItemCSVRegex.__init__(self, name, description, self.REPRESENTATION_RE, self.REPRESENTATION_RE_FLAGS, default=default, mandatory=mandatory, multivalue=multivalue)



    
    
class subscribable(object):
    def __init__(self, topic, qos=0, client=None, on_recieve=None, on_subscribe=None):
        self.sub_topic = topic  # TODO: namespace $FOO here
        self.sub_value = None
        self.sub_retained = None
        self.sub_time = None
        self.sub_qos = qos
        self.sub_granted_qos = None
        self.on_recieve = on_recieve
        self.on_recieve_lock = Lock()
        self.on_subscribe = on_subscribe
        if client:
            client.subscribe(self)
    
    def _on_subscribe(self):
        if self.on_subscribe:
            self.on_subscribe(self)
    
    def _recieve(self, client, userdata, mqtt_message):
        with self.on_recieve_lock:
            self.sub_value = mqtt_message.payload
            self.sub_retained = mqtt_message.retain
            if not self.sub_retained:
                self.sub_time = mqtt_message.timestamp
                
            if self.on_recieve:
                self.on_recieve(mqtt_message)
            
    def connect(self, client):
        client.subscribe(self)
    
class publishable(object):
    def __init__(self, topic, value=None, qos=0, retain=False, client=None):
        self.pub_topic = topic
        self._pub_value = None
        self.pub_qos = qos
        self.pub_retain = retain
        self.on_published = None
        self.queue = {}  # TODO: add queuing mechanism
        self.pub_client = client
             
    def _on_published(self):
        if self.on_published:
            self.on_published(self)
            
    def connect(self, client):
        self.pub_client = client
    
    @property
    def pub_value(self):
        return self._pub_value
    
    @pub_value.setter
    def pub_value(self, value):
        if not self.pub_client:
            raise NoClientException('Cannot publish because topic is not connected')
        self._pub_value = value
        self.pub_client.publish(self)
      
  
class pubsubtopic(publishable, subscribable):
    '''
    TODO: unneeded?
    '''
    def __init__(self, topic):
        publishable.__init__(self, topic)
        subscribable.__init__(self, topic)  
    
    @property
    def value(self):
        self.sub_value
        
    @value.setter
    def value(self, value):
        self.pub_value = value
        
    def connect(self, client):
        subscribable.connect(self, client)
        publishable.connect(self, client)
           

class interface(Configuration):
    META_ITEMS_INOUT = [
        ConfigItemString('path', 'mqtt topic (full path, no wildcards)', mandatory=True),
        ConfigItemString('title', 'A Human readable version of the name (Rightmost part of hierarchy)'),
        InterfaceDataTypes('data', 'describes the dataset transported in the mqtt payload'),
        ConfigItemCSV('fields', 'Field names for fields in data'),
        ConfigItemString('unit', 'The unit a value is in (use SI names where possible)'),
        ConfigItemInt('frequency', 'expected frequency of new messages in seconds'),
        ConfigItemFloat('tolerance', 'accuracy or significance of a change of value'),
        # TODO: Translations
                        ]
    
    
    META_ITEMS = [
                  ConfigItemString('path', 'Interface Path under meta', mandatory=True),
                  ConfigItemString('title', 'A Human readable version of the name (Rightmost part of hierarchy)'),
                  ConfigItemSubConfig('input', 'Subconfig if this topic can recieve', META_ITEMS_INOUT),
                  ConfigItemSubConfig('output', 'Subconfig if this topic can send', META_ITEMS_INOUT),
                  ConfigItemString('state', 'topic where to find the state of the interface', mandatory=False),
                  ]
    
    def __init__(self, path, data=None):
        Configuration.__init__(self, path, items=self.META_ITEMS)
        self.read(cache=False, use=[], config={'path':path})
        if data:
            self.parse(data)
        self.validate()
     
    def parse(self,data):
        self.log.debug(type(data))
        
        if isinstance(data ,str):
            json_string = data
        
        elif isinstance(data, bytearray):
            json_string = data.decode('utf-8')
        else:
            raise ConfigInvalidException('The data in the topic is not a String or Array of Bytes. You Shall Not PARSE!')
            
            
        config = self._deserialize_json(json_string)
        self.read(cache=False,use=[],reset=False,config=config)
        self.validate()
        
        
class interfaces(Configurable, subscribable, loggable):
    
    CONF_ITEMS = [
                  ConfigItemString('metatopic', 'Where to look for interface topics', default='meta'),
                  ] + loggable.CONF_ITEMS
    
    def __init__(self, namespace, client, config={}):

        self.interfaces = {}
        self.configure(config=config)

        self.metatopic = namespace + '/' + self.conf.metatopic + '/#'

        subscribable.__init__(self, self.metatopic, client=client, on_recieve=self.update_interface)
    
    def add_interface(self, interface):
        pass 
        
    def update_interface(self, mqtt_message):
        path = mqtt_message.topic.lstrip(self.metatopic)
        
        try:
            if path in self.interfaces:
                self.interfaces[path].parse(mqtt_message.payload)
            else:
                self.interfaces[path]=interface(path,data=mqtt_message.payload)
        except ConfigInvalidException as e:
            self.log.warn('The Interface Description at topic ' + mqtt_message.topic + ' is invalid')
            self.log.debug(e.message)
           
        
        
    def _update_children(self):
        pass
    
    def get_interface(self, path):
        pass


     
class client(Configurable, Thread, loggable):
    '''
    mqtt connection
    
    client id must be unique so can only use a base string + uuid?
    in hannah the user name is the basis of authorization (not the client id))
    
    connection is spawned off in a thread (process?) from here
    will always try to reconnect (incrementing tries) 
    will only give access to mqtt targets that match the hannah usage scheme
    
    Enforces security measures. user cannot "forget" to configure encryption/authentication because its to inconvenient

    TODO: add thread safety
    
    TODO: make context manager
    
    '''
    
    
    
    
    CONF_ITEMS = [
                 ConfigItemString('host', 'Host/IP of MQTT Broker', mandatory=True),
                 ConfigItemInt('port', 'Port at MQTT Broker', default=1883, mandatory=True),
                 ConfigItemInt('keepalive', 'Keepalive (ping) time', default=60, mandatory=True),
                 ConfigItemString('bindaddr', 'IP to use for outgoing connection'),
                 ] + loggable.CONF_ITEMS
    
    def __init__(self, config={}):
        super(client, self).__init__()
        self.errors = []
        self._subscriptions = []
        self._mqtt_thread = None
        self.mqttc = mc.Client(userdata=self)
        self.connected = Event()
        self.autoconnect = False
        self.on_connect = None
        self.on_error = None
        self.on_log = None
        self.on_message = None
        self.on_conf_ready = self._on_conf_ready
        self.on_conf_error = None
        self.configure(validate=False, config=config)
        self._subscribing = {}
        self._subscribing_mutex = Lock()
        self._publishing = {}
        self._publishing_mutex = Lock()
    
    def __enter__(self):
        self.connect(True)
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()
    
            
    def _on_error(self, exc, re_raise=True):
        self.errors.append(exc)
        self.error(exc)
        if exc and re_raise:
            raise exc
        
    def _on_log(self, client, userdata, level, buf):
        self.debug(buf)
        
    def _on_conf_ready(self):
        self.debug('Configuration ready')
        if self.autoconnect:
            self._connect()    
        
    def connect(self, block=True, timeout=60):
        self.autoconnect = True
        if self.conf.valid:
            self._connect()
        else:
            self.conf.validate()
        if block:
            self.connected.wait(timeout)
                    
    def _connect(self, blocking=False):
        self.mqttc.on_disconnect = self._on_disconnect  
        self.mqttc.on_log = self._on_log
        self.mqttc.on_connect = self._on_connect
        self.mqttc.on_subscribe = self._on_subscribe
        self.mqttc.on_publish = self._on_publish
        self.mqttc.connect_async(self.conf.host, keepalive=self.conf.keepalive,)
        self.debug('starting connection loop thread')
        self.start()
    
    
    class async_callable(object):
        def __init__(self, function):           
            self.function = function
            self.__code__ = function.__code__
        
        def __call__(self, *args):
            thread = Thread(target=self.function, args=args)
            thread.start()

    @async_callable
    def _on_connect(self, client, flags, result):
        '''
        Implements mqtt.Client on_connect. So self is not an Instance of this Object but of the mqtt client (mqttc)
        We send the instance of this Object as userdata to the mqtt.Client Object thus receiving it here.
        '''
        client.debug('Starting on_connect')
        client.connected.set()
        client.info('Connected to mqtt broker ' + client.conf.host)
        for subscribable in client._subscriptions:
            client._subscribe(subscribable)
        if client.on_connect:
            client.on_connect()
        client.debug('exit on_connect')      
 
    def disconnect(self):
        self.mqttc.disconnect()
    
    @async_callable
    def _on_disconnect(self, client, flags, rc):
        client.connected.clear()
        client.info('Disconnected from broker ' + client.conf.host)
         
    def subscribe(self, subscribable):
        self.debug('subscribing to ' + subscribable.sub_topic)
        self._subscriptions.append(subscribable)
        self.mqttc.message_callback_add(subscribable.sub_topic, subscribable._recieve)
        self._subscribe(subscribable)
        
    def _subscribe(self, subscribable):
            if self.connected.is_set():
                with self._subscribing_mutex:
                    self.debug('Subscribing to topic: ' + subscribable.sub_topic)
                    (result, mid) = self.mqttc.subscribe(subscribable.sub_topic, subscribable.sub_qos)
                    if result == mc.MQTT_ERR_SUCCESS:
                        self._subscribing[mid] = subscribable
                    else:
                        raise Exception('Could not subscribe to ' + subscribable.sub_topic + ' Error(' + result + '):' + mc.error_string(result))
                self.debug('subs')
    
    @async_callable       
    def _on_subscribe(self, client, mid, granted_qos):
        client.debug('entering on_subscribe')
        try:
            with client._subscribing_mutex:
                subscribable = client._subscribing.pop(mid)
            subscribable.sub_granted_qos = granted_qos
            client.debug('Subscribed to ' + subscribable.sub_topic) 
            subscribable._on_subscribe()
        except(Exception) as e:
            client._on_error(e)
        client.debug('exit on_subscribe')

   
    def publish(self, publishable):
        if not self.connected.is_set():
            raise NotConnectedException('Client Not Connected')
        with self._publishing_mutex:
            self.debug('Publishing ' + publishable.pub_topic)
            (result, mid) = self.mqttc.publish(publishable.pub_topic, payload=publishable.pub_value, retain=publishable.pub_retain, qos=publishable.pub_qos)
            if result == mc.MQTT_ERR_SUCCESS:
                self._publishing[mid] = publishable
            else:
                raise PublishException('Could not publish ' + publishable.pub_topic + ' Error(' + result + '):' + mc.error_string(result))
    
    @async_callable     
    def _on_publish(self, client, mid):
        client.debug('Entering on_publish')
        try:
            with client._publishing_mutex:
                publishable = client._publishing.pop(mid)
            client.debug('Published' + publishable.pub_topic)
            publishable._on_published()
        except(Exception) as e:
            client.on_error(e)
        client.debug('Exiting on_publish')


    
    def run(self):
        try:
            self.mqttc.loop_forever()
        except(Exception) as e:
            self._on_error(e) 
     
     
