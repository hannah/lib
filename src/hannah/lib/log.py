'''
Created on 8 May 2015

@author: wonko
'''

import logging
import inspect
import hannah.lib.config as hc


def log_to_stderr(logger=None, level=logging.DEBUG):
        if not logger:
            logger = logging.getLogger()
        logger.setLevel(level)
        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s - File "%(pathname)s", line %(lineno)d ')
        ch.setFormatter(formatter)
        logger.addHandler(ch)


class modulelogger(logging.Logger):
    '''
    Easy logging for a Class.
    Subclass loggable and use.
    A special treat is develop() which can be used as a replacement for print() during development.
    '''
    
    def __init__(self, module='unknown module'):
        for s in inspect.stack():
            frame = s[0]
            frame_module = inspect.getmodule(frame).__name__
            if frame_module != modulelogger.__module__:
                module = frame_module
                break
        logging.Logger.__init__(self, module)
        self.enabled = {}
    
    def configure(self,conf):
        self.setLevel(conf.log_level)
        if conf.log_stderr:
            self.to_stderr()
    
    def to_stderr(self):
        log_to_stderr(self)
    
    def isEnabledFor(self, level, cache=False):
        if cache:
            try:
                return self.enabled[level]
            except(KeyError):
                enabled = self.isEnabledFor(level)
                self.enabled[level] = enabled
                return enabled
                
        return logging.Logger.isEnabledFor(self, level)
    
       
    def develop(self, msg):
        if not hasattr(self, '_develop_logger'):
            logger = modulelogger(module=self.name + '-develop')
            logger.propagate = False
            self.enabled = {}
            log_to_stderr(logger)
            self._develop_logger = logger
        else:
            logger = self._develop_logger
        
        (frame, file, line, function, context, context_line) = inspect.stack()[1]
        rv = { 'name':logger.name, 'levelno':logging.DEBUG, 'levelname':'DEVEL', 'pathname':file, 'lineno':line, 'msg':msg}
        record = logging.makeLogRecord(rv)
        logger.handle(record)
        
    def _dont(self,*args,**kwargs):
        pass          

    @property
    def debug(self):
        if self.isEnabledFor(logging.DEBUG,cache=True):
            return super(modulelogger, self).debug
        else:
            return self._dont
        
    @property
    def info(self):
        if self.isEnabledFor(logging.INFO,cache=True):
            return super(modulelogger, self).info
        else:
            return self._dont

class loggable(object):
    
    CONF_ITEMS = [hc.ConfigItemLogLevel('log_level','Log Level', default=logging.INFO),
                  hc.ConfigItemBoolean('log_stderr','Logging to stderr in addition to global loging conf', default=False),]
    
    @property
    def log(self):
        try:
            self._logger
        except:
            self._logger = modulelogger()
            self._logger_configured = False
              
        if not self._logger_configured:
            if isinstance(self,hc.Configurable):
                if hasattr(self, 'conf') and self.conf.valid:
                    try:
                        self._logger.configure(self.conf)  
                    except(AttributeError):
                        self._logger.info('This class is of type configurable and loggable but does not provide logging config items')
                    self._logger_configured = True
            else:
                self._logger_configured = True
        return self._logger
               
    @property
    def debug(self):
        return self.log.debug
        
    @property
    def info(self):
        return self.log.info
    
    @property
    def warning(self):
        return self.log.warn
    
    @property
    def error(self):
        return self.log.error

    @property
    def critical(self):
        return self.log.critical

    @property
    def exception(self):
        return self.log.exception
    
    @property
    def develop(self):
        return self.log.develop
