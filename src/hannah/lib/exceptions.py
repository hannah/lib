import traceback

class MultipleExceptions(Exception):
    class inner(object):
        def __init__(self, outer):
            self._outer = outer
        def __enter__(self):
            return self            
        def __exit__(self, exc_type, exc_value, exc_tb):
            self._outer.add_reason(exc_type, exc_value, exc_tb)
            return True
        
    def __init__(self, message=''):
        self._reasons = []
        self.message = message
        self._iscontexthandler = False

    
    def __enter__(self):
        self._iscontexthandler = True
        return self.inner(self)
    
    def __exit__(self, exc_type, exc_value, traceback):
        if self._iscontexthandler and self.has_reasons():
            raise self
    
    def add_reason(self, exc_type, exc_value, exc_traceback):
        if exc_value:
            self._reasons.append((exc_type, exc_value, exc_traceback))
    
    def has_reasons(self):
        return  len(self._reasons) is not 0
    
    def __str__(self, level=None):
        if self.has_reasons():
            if level:
                _level = level +'\t'
                s = [self.message.__str__(),]  
            else:
                _level = '\t'
                s = [self.__class__.__name__ + ': ' + self.message.__str__(), ]
            for (exc_type, exc_value, exc_tb) in self._reasons:
                if issubclass(exc_type,self.__class__):
                    s.append(_level + exc_type.__name__ + ': ' + exc_value.__str__(level=_level))
                else:
                    s.append(_level + exc_type.__name__ + ': ' + exc_value.__str__())
                tblines = _level.join(traceback.format_tb(exc_tb))
                for line in tblines.splitlines():
                    s.append(_level + line)         
            return '\n'.join(s)
        else:
            return self.message


