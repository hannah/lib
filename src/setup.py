from setuptools import setup

setup(name='hannah-lib',
      version='0.0.1-dev1',
      description='Hannah Lib',
      url='http://doc.hannah.cat',
      author='The Hannah Project',
      author_email='dev@hannah.cat',
      license='GPLv3+',
      packages=['hannah.lib'],
      zip_safe=False,
      classifiers=[
            'Development Status :: 3 - Alpha',
            'Intended Audience :: Developers',
            'Topic :: Software Development :: Libraries',
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            'Programming Language :: Python :: 3',
            ],
      install_requires=['paho-mqtt>=1.1'],
      tests_require=['nose>1.3'])
